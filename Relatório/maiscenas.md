**Deverão apresentar alternativas e comparação de alternativas**

No que diz respeito às alternativas que poderíamos tomar no desenvolvimento do processo, realçam-se as seguintes:

- Fase de receção e confirmação de encomenda:
	- Antes da confirmação da encomenda e, caso o gestor optasse por requerer, nomeadamente, a confirmação de existência de stock e de matérias primas, a estimativa de tempo e custo de produção, poderíamos optar por considerar estas tasks como "Service tasks", em vez de "User Tasks". Assim, o processo tornar-se-ia automático e o próprio ator não teria qualquer influência sob o processo.


- Fase de conclusão da ordem de encomenda
	-  Aquando da recolha do material para satisfazer a encomenda, poderíamos também substituir a "User Tasks" por uma "Service Task", na medida em que apenas avisa que a recolha tinha sido efetuada.
