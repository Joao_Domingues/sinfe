Explicação da abordagem ao processo e justificação das opções tomadas

O processo engloba 3 grandes entidades principais - o cliente, a organização e o(s) fornecedor(es) - que foram destacadas em 3 pools distintas.

Mapeou-se, para cada task, qualquer que fosse o seu tipo, um caso de uso de entre os inicialmente identificados

// Pools, Lanes & Sub Lanes

A abordagem ao processo começou pela definição das entidades envolvidas e respetivos constituintes.

Foram identificadas 3 entidades independentes - Cliente, Fornecedor e Organização (representada pelo Software de Gestão de Ordens de Encomendas).

Foram ainda identificados os componentes que constituem estas entidades, nomeadamente, a Organização, uma vez que as outras duas são apresentadas como fatores externos ao processo descrito no enunciado.

Os componentes identificados foram:
  - Departamento de Vendas;
  - Departamento Comercial;
  - Departamento de Armazém e Distribuição;
  - Departamento de Produção;
  - Departamento Financeiro;
  - Departamento de Aprovisionamento;
  - Departamento de Qualidade;
  - Shop Floor;

Por fim, foram identificados os atores envolvidos em cada um destes componentes.

Na modelação do BPMN, o mapeamento entre os termos até agora identificados e elementos de BPMN foi feita da seguinte forma:
  - Pools representam uma entidade;
  - Lanes representam componentes de uma entidade;
  - Sub Lanes representam os atores envolvidos no processo descrito.

// Tasks e Sub-Tasks

Após este processo inicial, foi feita a identificação de potenciais Casos de Uso, fazendo-se a correspondência com os respetivos atores. Foi criado um Diagrama de Casos de Uso para representar este processo.

Adicionalmente, foi gerado um SSD para cada caso de uso, para se ter uma noção do mecanismo interno expectável de cada um deles, de forma a averiguar qual a melhor representação no diagrama BPMN final.

Na representação final, mapeou-se cada Caso de Uso para uma ou mais Tasks, que foram colocadas na sub-lane do respetivo ator.

O tipo de task utilizada foi derivada da natureza do caso de uso.
  - Casos de uso que se restringiam a uma simples interação (I/O) com o utilizador resultaram numa User Task;
  - Casos de uso tidos como automáticos - o ator é o sistema em si - foram representados por um Service Task;
  - Casos de uso que envolvessem qualquer tipo de processamento externo e/ou um fluxo de tomadas de decisão por parte do ator foram convertidos em várias tasks.

Uma vez que não existe nenhuma fase do processo que é repetida em várias partes do mesmo, não existe nenhum sub-processo definido.

// Message/Sequence Flows

O fluxo entre as diferentes tasks seguiu a ordem cronológica apresentada na descrição do processo. Por outras palavras, se uma determinada parte do processo antecede outra, faz-se então uma ligação entre estas duas, com sentido da anterior para a posterior.

Um detalhe a frisar é a distinção feita entre comunicação de partes do processo dentro de uma mesma entidade e entre entidades diferentes. Enquanto que comunicação interna à organização é representada por um Sequence Flow, as comunicação externas são representadas por Message Flows, uma vez que se fazem entre Pools distintas.

// Intermediate Events (Message, Timer & Signal)

Foram utilizados 3 tipos de Intermediate Events, cada um com um contexto de utilização próprio:
  - Message Events, que foram usados para representar qualquer tipo de comunicação que tivesse alguma informação (i.e.: payload) associada;
  - Timer Events, que foram usados para representar limites no tempo de resposta;
  - Signal Events, que foram usados para representar uma chamada de outra parte do processo ainda dentro de um sub-processo. Foram usados 2 tipos de Signal Events - Throwing e Catching. Quando se utiliza um Signal Thrower, o processo que efetua a chamada continua a correr, sem esperar pela resposta do processo chamado, daí a existência de um Signal Catcher após cada Signal Thrower, de modo a que o processo que efetua a chamada aguarde por algum tipo de resposta.

// Gateways

Passos do processo que envolvessem uma tomada de decisão por parte do sistema foram representados por um Exclusive Gateway, a partir do qual saia um Sequence Flow por cada outcome possível.

// Loop-types (Multi instance, Standard Loop)

Por fim, foram detetados alguns passos individuais do processo que eram repetíveis ou executáveis por várias pessoas em paralelo. Estes foram representados por Tasks que tinham a sua propriedade Loop-Type definida como Standard Loop ou Multi Instance, respetivamente.
